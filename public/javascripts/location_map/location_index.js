$( document ).ready(function(){
  console.log("Drawing map started");
  var container = document.getElementById('container');
  var map = new Datamap({
    element: container,
    
    geographyConfig: {
      dataUrl: 'custom.json',
      borderWidth: 1,
      borderColor: 'black',
      popupTemplate: function(geography, data) { //this function should just return a string
          return '<div class="hoverinfo"><strong>' + geography.properties.name +' '+ data.info+ '</strong></div>';
      },
        popupOnHover: true, //disable the popup while hovering
            highlightOnHover: true,
            highlightFillColor: 'orange',
            highlightBorderColor: 'rgba(0, 255, 0, 0.2)',
            highlightBorderWidth: 1      
    },
     bubblesConfig: {
        borderWidth: 2,
        borderColor: '#FFFFFF',
        popupOnHover: true,
        popupTemplate: function(geography, data) {
          return '<div class="hoverinfo"><strong>' + data.name + '</strong></div>';
        },
        fillOpacity: 1.75,
        animate: true,
        highlightOnHover: true,
        highlightFillColor: '#FC8D59',
        highlightBorderColor: 'rgba(250, 15, 160, 0.9)',
        highlightBorderWidth: 2,
        highlightFillOpacity: 0.85,
        exitDelay: 100
    },

    scope: 'house',
    
    fills: {
      defaultFill: 'green',
      low: 'yellow',
      high: 'red',
      medium: 'green'
    },
    
    data: {
      bed1: {fillKey: 'low',  info: "lastly occupied at:" },
      bed2: {fillKey: 'high',  info: "lastly occupied at:"},
      living: {fillKey: 'medium', info: ""},
      kitchen: {fillKey: 'high', info: ""}

    },
    
    setProjection: function(element) {
      var projection = d3.geo.mercator()
        .center([0, 0])
        .scale(1000)
        .translate([0, 0]);

       var path = d3.geo.path().projection(projection);
       return {path: path, projection: projection};
    }
  });

  var numberOfItems = 5;
  var rainbow = new Rainbow(); 
  rainbow.setNumberRange(1, numberOfItems);
  rainbow.setSpectrum('yellow', 'red');
  colour_range = Array();
  for (var i = 1; i <= numberOfItems; i++) {
    var hexColour = rainbow.colourAt(i);
    colour_range.push('#'+hexColour);
  }
  console.log(colour_range); 



//////////////////////////bubbles

  var bubbles = [{
    name: 'point1',
    radius: 5,
  
    fillKey: 'high',
    latitude: -2.07,
    longitude: 1
    
  },{
    name: 'point2',
    radius: 5,
    fillKey: 'high',
    significance: 'test2',
    latitude: -4.07,
    longitude: 1.43
    }
  ];
 
  //draw bubbles for bombs
  map.bubbles(bubbles);

  window.setTimeout(function() {
      var bubbles = [{
        name: 'point1',
        radius: 5,
      
        fillKey: 'high',
        latitude: -2.07,
        longitude: 3
        
      },{
        name: 'point2',
        radius: 5,
        fillKey: 'high',
        significance: 'test2',
        latitude: -8.07,
        longitude: 1.43
        }
      ];

      map.bubbles(bubbles, {exitDelay: 1000});    
      console.log("called");
  }, 2000);


});



 
