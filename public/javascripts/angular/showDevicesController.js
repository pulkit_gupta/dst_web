<!--
 /*
  Function: it show all devices
  Interacts with DB: No
  Entry point: internal

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

-->
dstApp.controller('showDevicesController', function($scope, $location, socket, server, userInfo, $route) {
    console.log("started device controller");
    

 

      var getLedColor = function(status){
          if(status=="Registered"){ return "led-blue";}
          else if(status=="Offline"){ return "led-orange";}
          else if(status=="Connecting"){return "led-yellow";}
          else if(status=="Error"){ return "led-red";}
          else if(status=="Connected"){ return "led-green";}
        } 
      //get the values from data base
      $scope.columns = ["Device-ID", "IP-Address", "Type", "Livestream", "Actions" ,"", "Status"];

      // Connected -> Green, Registered -> Blue, Offline: "Yellow", Error: "red" Connecting: "Blink green"

      
      var data = [
              {device_data: ["APK112", "168.1.1", "Framework", "Yes", "*actions", "*led", "Registered"], led: "led-blue"},
              {device_data: ["XPK113", "168.1.2", "Framework", "Yes", "*actions", "*led", "Offline"], led: "led-orange"}
              ];
      $scope.data = data;           
      
      //socket connected will give the id of socket and then server will set the $scope.led_status array with either offline or connected values
      // 

      
      $scope.update_status = function(device_id, live_status){
        for(var i = 0; i<$scope.data.length; i++){
          if($scope.data[i].device_data[0]==device_id){
            $scope.data[i].device_data[6] = live_status;
            $scope.data[i].led = getLedColor(live_status);

            console.log("Device is found and GUI text with led color is changed to "+live_status);
          }
        }
      };

      var init = function(){       
         $("#myTable").tablesorter();        
      }

      setTimeout(init, 1000);



});