<!--
 /*
  Function: Main Includes and file ffor angular app
  Interacts with DB: No
  Entry point: internal

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

-->

dstApp.controller('memberController', function($scope, $location, socket, userInfo, $route, Fullscreen) {			

	   
	if(userInfo.session==false)
	{
		$location.path( "/login" );
	}
	/* 
	scope variables are

	1. menu[0] only gives tab visual effect for css property (only in header.html)
	2. menu_type is used to include different icons (dashboard.html and admin.html to eliminate the accuont info on about)
		menu_type also changes the tabView implictly

	3. tabView is used to tell the page to be displayed. Its the outermost in admin.html and will be extended
	*/

	/* initial page*/	
	$scope.tabView = "icons"; 
	/* For different functionality */
	var root ="/pages/partials/";
	
	$scope.url= {header: root+"header.html", footer: root+"footer.html", dashboard: root+"dashboard_icons.html",
	account_overview: root+"account_overview.html", about: root+"about.html",
	graph1: root+"graph1.html", graph2: root+"graph2.html", graph3: root+"graph3.html", livestream: root+"livestream.html",
	heatmap: root+"heatMap.html", showDevices: root+"showDevices.html"
	};


	/*
		account info
	
	$scope.user = userInfo.name;
	$scope.ip= userInfo.ip;	
		
	$scope.filled = userInfo.db_filled;
	$scope.dbval=60;
	$scope.dblastupdated = "12th may 2014";
	*/




	/*for menu tabs*/
	var menu_num = 5;
	$scope.menu_type = "0";
	$scope.menu = new Array();
	for (var i = 0; i < menu_num; i++) {
		$scope.menu.push("0");		
	};
	$scope.menu[0]= "current"; //default
	


	$scope.updateMenu = function(index)
	{	
		/* For dashboards icon*/
		$scope.menu_type = index.toString();	
		/* For different functionality hardcoded with menu*/
		var tabs_mapping = ['icons', 'icons', 'icons', 'contact', 'about' ];
		$scope.tabView = tabs_mapping[index];
		for (var i = 0; i < menu_num; i++) {				
			if(i === index)
				$scope.menu[i]= "current";	
			else
				$scope.menu[i] = "0";		
		};		
	}
	
	
	$scope.changeView = function(value)
	{
		$scope.tabView = value;
	}

	console.log("i need object now in controlller");
	var socket = socket.getSocket();
/*	socket.on('pong', function () {
		console.log('- pong');
	});	
	
	socket.on('time', function (data) {
		console.log('- broadcast in about: ' + data);
	});
	socket.on('authenticated', function () {
		console.log('- authenticated');
	});
	socket.on('disconnect', function () {
		console.log('- disconnected');
	});
*/
	$scope.$on('$destroy', function (event) {
		
		console.log("leaving member controller and all socket removed");
		//socket.getSocket.removeAllListeners();
		// or something like
		// socket.removeListener(this);
    });
	$scope.logout = function(){
		console.log("logging out. :)");
		if(userInfo.session)
		{
			socket.removeAllListeners();
			socket.disconnect();
		}
		userInfo.session = false;
		userInfo.token = '0';
				
		$location.path( "/login" );
		$route.reload;
	};




	$scope.goFullscreen = function () {

	     // Fullscreen
	     if (Fullscreen.isEnabled())
	        Fullscreen.cancel();
	     else
	        Fullscreen.all();

	     // Set Fullscreen to a specific element (bad practice)
	     // Fullscreen.enable( document.getElementById('img') )

	  };

	$scope.isFullScreen = false;

	$scope.goFullScreenViaWatcher = function() {
	   $scope.isFullScreen = !$scope.isFullScreen;
	};

});