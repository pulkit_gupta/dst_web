 /*
  Function: Provide all services like global var management service, UserRecord service, Socket and ajax communiocation and authentication
  Interacts with DB: Yes
  Entry point: Ajax communication

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com

 */
	

dstApp.service('userInfo', function(){
	console.log("calling client global var service");
	return{ token: '0',
			sessionData:{
				//graph2: {start: moment("19/06/2014 20:00:00", "DD/MM/YYYY HH:mm:ss"),stop: moment("20/06/2014 20:00:00", "DD/MM/YYYY HH:mm:ss"), series: ""},
				graph2: {start: moment("18/08/2014 16:00:00", "DD/MM/YYYY HH:mm:ss"),stop: moment("01/10/2014 23:00:00", "DD/MM/YYYY HH:mm:ss"), series: ""},
				heatMap: {start: moment("08/09/2014 16:00:00", "DD/MM/YYYY HH:mm:ss"),stop: moment("21/09/2014 23:00:00", "DD/MM/YYYY HH:mm:ss"), data: {eventsList: "", series: "", design: ""}},
				graph3: {start: moment("18/08/2014 16:00:00", "DD/MM/YYYY HH:mm:ss"),stop: moment("01/10/2014 23:00:00", "DD/MM/YYYY HH:mm:ss"), series: ""},
				//in g1 series contain both seris array and category x axis array
				graph1: {start: moment("10/08/2014 18:00:00", "DD/MM/YYYY HH:mm:ss"),stop: moment("10/09/2014 23:59:59", "DD/MM/YYYY HH:mm:ss"), series: "", interval_mode: 3}
			},
			session: false, 
			name: "Pulkit", 
			ip: "134.104.5.21", 
			db_filled: 23, 
			db_value: 100, 
			last_updated: "13th may 2014"
			/*
			data: "", //JSON array of rows
			event_table: {ev_name: "", num_of_events: "", id_array: ""}
			*/
		
		};
});
//1403434662010
//getHeatPoints


dstApp.service('server', function($http, userInfo){
	console.log("calling server service");

	return{ 
		get_heatMapPoints: function(callback){
			var req = { "token": userInfo.token};
			$http({ method: "POST", url: "/getHeatPoints", data: req }).success(function (resp) {
				if(resp.data_error)
				{
					console.log("No data recieved, dummy data is displayed.");
				}
				else
				{
					console.log("value recieved for heatmap points are ");										
					userInfo.sessionData.heatMap.data.series = resp.series;
					userInfo.sessionData.heatMap.data.eventsList = resp.name_array;
					console.log(userInfo.sessionData.heatMap.data.series);
					callback(userInfo.sessionData.heatMap.data);
				}
			});
		},
	       
		get_heatMap: function(callback){
			var req = { "token": userInfo.token};
			$http({ method: "POST", url: "/getHeatMap", data: req }).success(function (resp) {
				if(resp.data_error)
				{
					console.log("No data recieved, dummy data is displayed.");
				}
				else
				{
					console.log("value recieved for heatmap. and first element of design look like this");										
					userInfo.sessionData.heatMap.data.design = resp.design;			     
					console.log(userInfo.sessionData.heatMap.data.design[0]);
					
					callback();
				}
			});
		},

		get_series3: function(callback){
			var req = { "token": userInfo.token};
			$http({ method: "POST", url: "/getDataG3", data: req }).success(function (resp) {
				if(resp.data_error)
				{
					console.log("No data recieved, dummy data is displayed.");
				}
				else
				{
					console.log("value recieved for graph3 is ");					
			        userInfo.sessionData.graph3.series = resp; //stored in session listing          			        
			        callback();
			    }
			});
		},

		get_series2: function(callback){
			var req = { "token": userInfo.token};
			$http({ method: "POST", url: "/getDataG2", data: req }).success(function (resp) {
				if(resp.data_error)
				{
					console.log("No data recieved, dummy data is displayed.");
				}
				else
				{
					console.log("value recieved for graph2 is ");					
			        userInfo.sessionData.graph2.series = resp; //stored in session listing          			        
			        callback();
			    }
			});
		},

		get_series1: function(info, callback){
			var req = { "token": userInfo.token, "info": info};
			$http({ method: "POST", url: "/getDataG1", data: req }).success(function (resp) {
				if(resp.data_error)
				{
					console.log("No data recieved from G1, dummy data is displayed.");
				}
				else
				{
					console.log("value recieved for graph1 is ");					
			        userInfo.sessionData.graph1.series = resp; //stored in session listing  with subvar cat and series			        
			        callback(resp);
			    }
			});
		}

	};
});




dstApp.factory('socket', function ($rootScope, $http, userInfo) {

	//Singleton object

	return {
		getSocket: function() {

			console.log("value from storage is "+ userInfo.token +" now asking for handshake");
			var socket =  socket = io.connect('', {
				'force new connection': true,
				query: userInfo.token ? 'token=' + userInfo.token : undefined          
			});
			
			console.log("object made for socket handshake");		
			return socket;
		}
	};

});

