/*
  
  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */
var dummy_tasks = [{
              name: 'Balcony',
              intervals: [
              ]},

              {
              name: 'Outside',
              intervals: [{
                  from: 1402468800000,
                  to: 1402502400000
              }
              ]},{
              name: 'Corridor',
              intervals: [{
                  from: 1402468200000,
                  to: 1402468800000,
              },{
                  from: 1402502400000,
                  to: 1402502700000,
              }]},

              {
              name: 'Kitchen',
              intervals: [{
                  from: 1402465200000,
                  to: 1402466400000,
              },{
                  from: 1402504200000,
                  to: 1402506000000,
              },{
                  from: 1402509600000,
                  to: 1402510500000
              }]},
              {
              name: 'Bathroom',
              intervals: [{
                  from: 1402463400000,
                  to: 1402464600000,
              },{
                  from: 1402503300000,
                  to: 1402504200000,
              },{
                  from: 1402510500000,
                  to: 1402511400000
              }
              ]},
              {
              name: 'Living room',
              intervals: [{
                  from: 1402462800000,
                  to: 1402463400000,
              },{
                  from: 1402464600000,
                  to: 1402465200000,
              },{
                  from: 1402466400000,
                  to: 1402468200000
              }, {
                  from: 1402502400000,//from: 1402506000000,
                  to: 1402509600000
              }]
          },{
              name: 'Bedroom',
              intervals: [{
                  from: 1402437600000,
                  to: 1402462400000,
              },{
                  from: 1402511400000,
                  to: 1402524000000,
              }]
          }];