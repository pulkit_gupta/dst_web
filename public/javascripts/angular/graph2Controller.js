 /*
  Function: initiate all operation for event gannt chart
  Interacts with DB: Yes
  Entry point: Launch()

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com

 */


 dstApp.controller('graph2Controller', function($scope, $location, socket, userInfo, server, $route, $http) {

   //var chart;

   
    var startDateTimeStr = userInfo.sessionData.graph2.start.format("DD/MM/YYYY HH:mm:ss");
    var stopDateTimeStr = userInfo.sessionData.graph2.stop.format("DD/MM/YYYY HH:mm:ss");
    $scope.rangeStamp = startDateTimeStr+ " - " +  stopDateTimeStr;    


    /*
     Generation
    /////////////////////////////////////////////////////////////////////////*/
    
    //////////////////////////////////////////////////////////////////////////////
    /*
        Controller logics and scope variables
    */
    var intervalID = 0, led_intervalID= 0;
    $scope.autosync = 1;    
    $scope.graph2_timer_value = 5;

        
    $scope.change_graph2_timer = function()
    {      
      $scope.autosync = 0;
      
      if($scope.graph2_timer_value % 1 === 0)
      {
         console.log("value of scope variable is "+ $scope.graph2_timer_value);      
      } 
      else
      {
        $scope.graph2_timer_value = 5;
        console.log("enter a valid value");

      }
      $scope.graph2_timer();
      $scope.autosync = 1;

    }

    $scope.graph2_timer = function(){
      console.log("checkbox clicked and value is "+ $scope.autosync+ "and check button value is"+ $scope.check_button);

      if($scope.autosync == 1)
      {
        var n1 =  $('#noty_area').noty({ dismissQueue: true, maxVisible: 50,  maxVisible: 50, timeout: 5000, template: '<span class="notification n-attention">Choosing autosync can drain lot of data if you are running on 3G.</span>'});
          intervalID = setInterval(function(){           
            launch();
        }, $scope.graph2_timer_value*1000);   

            led_intervalID = setInterval(function(){
              $('.led1').toggleClass('led-green');              
          }, 500);   
      }  
      else
      {
         clearInterval(intervalID);
          clearInterval(led_intervalID);
      } 
    }

    $scope.$on('$destroy', function (event) {
      
      console.log("leaving graph2 controller and all intervals removed");          
      $scope.autosync = 0;
       $scope.graph2_timer();

      
      console.log("removed all");
      // or something like
      // socket.removeListener(this);
    });


    /*
        Launch to get  data sync and draw graph
    
    */
    var launch = function()
    {
      console.log("calling launch")
      server.get_series2(function(){         
            
              console.log("REPLY :)");
              //var tasks = dummy_tasks;  //from file         
            
              var tasks = userInfo.sessionData.graph2.series;  //from file 
              console.log("i am task and have data =>");
              console.log(tasks);        
              //var tasks = dummy_tasks;
              //console.log("Dummy task and i have  data =>");
              //console.log(tasks);        
              
              // re-structure the tasks into line seriesvar series = [];

              var milestones = [];

              // re-structure the tasks into line seriesvar series = [];
              var series = [];
              $.each(tasks.reverse(), function (i, task) {
                  var item = {
                      name: task.name,
                      data: []
                  };
                  $.each(task.intervals, function (j, interval) {
                      item.data.push({
                          x: interval.from,
                          y: i,
                          label: interval.label,
                          from: interval.from,
                          to: interval.to
                      }, {
                          x: interval.to,
                          y: i,
                          from: interval.from,
                          to: interval.to
                      });

                      // add a null value between intervals
                      if (task.intervals[j + 1]) {
                          item.data.push(
                          [(interval.to + task.intervals[j + 1].from) / 2, null]);
                      }

                  });

                  series.push(item);
              });

              
              // create the chart
              chart2 = new Highcharts.Chart({

                  
                  chart: {
                      renderTo: 'chartDiv'

                  },
                  credits: {
                   enabled: false
                  }, 
                  title: {
                      text: 'Events of user: '+ userInfo.name
                  },

                  xAxis: {
                     title: {
                          text: 'Time-line'
                      },
                      type: 'datetime',
                      min: moment(userInfo.sessionData.graph2.start).valueOf(),//new Date('2014/10/22').getTime(),
                      max: moment(userInfo.sessionData.graph2.stop).valueOf()//new Date('2014/10/22').getTime(),
                          
                  },

                  yAxis: {
                      tickInterval: 1,
                      labels: {
                          formatter: function () {
                              if (tasks[this.value]) {
                                  return tasks[this.value].name;
                              }
                          }
                      },
                      startOnTick: false,
                      endOnTick: false,
                      title: {
                          text: 'Location/Place/Room'
                      },
                      minPadding: 0.2,
                      maxPadding: 0.2
                  },

                  legend: {
                      enabled: false
                  },

                  tooltip: {
                      crosshairs: true,
                      formatter: function () {
                          return '<b>' + Highcharts.dateFormat('%Y-%m-%d, %H:%M:%S %p', this.point.options.from) + ' to ' + Highcharts.dateFormat('%Y-%m-%d, %H:%M:%S %p', this.point.options.to) + '</b><br/>';
                      }
                  },
                  

                  plotOptions: {
                      line: {
                          lineWidth: 9,
                          marker: {
                              enabled: false
                          },
                          dataLabels: {
                              enabled: true,
                              align: 'left',
                              formatter: function () {
                                  return this.point.options && this.point.options.label;
                              }
                          }
                      },
                      // clickable row
                      series: {
                          cursor: 'pointer',
                          point: {
                              events: {
                                  click: function () {
                                      //var options = this.series.options;
                                      alert('clicked on ' + Highcharts.dateFormat('%Y-%m-%d', this.from) + ' to ' + Highcharts.dateFormat('%Y-%m-%d', this.to));
                                  }
                              }
                          }
                      }
                  },

                  series: series

              });    


      });
    }

   

   /* First time draw, not too early ;)*/
   $(document).ready(function(){      

       launch();   
       //timer("set", 10000);
      // $scope.timer();
   }); //doc ready
  

  var create_interval_graph2 = function()
  {
      
      $('#range_graph2').daterangepicker(
      {
          timePicker: true,
          timePickerIncrement: 1,          
          format: 'DD/MM/YYYY HH:mm:ss',    
          pick12HourFormat: false,        
          //06/19/2014 22:00 - 06/20/2014 22:00
          startDate: userInfo.sessionData.graph2.start,//startDateTimeStr,//'19/06/2014 22:00:00',
          endDate: userInfo.sessionData.graph2.stop//stopDateTimeStr//'20/06/2014 22:00:00'
      }, 
      function(start, end, label) {
         console.log("CALLED"); 
         
         console.log(start.date()+"-"+(start.month()+1)+"-"+start.year()+"--"+start.hour()+":"+start.minute());


        console.log("end time is "+ end.date()+"-"+(end.month()+1)+"-"+end.year()+"--"+end.hour()+":"+end.minute());         

         userInfo.sessionData.graph2.start = this.startDate;
         userInfo.sessionData.graph2.stop = this.endDate;

         //console.log("value saved is "+ userInfo.sessionData.graph2.start+ " and end time is "+ userInfo.sessionData.graph2.stop);
         //chart2.xAxis[0].setExtremes(start+2*3600000, end);
         start.zone(0);
         end.zone(0);
         console.log("after changing zone to 0");
         console.log("timestamp start is"+ start);
         console.log("timestamp end is"+ end);
         chart2.xAxis[0].setExtremes(start, end);
     });
  };
  /*   Don't forget me, i am also important :)*/
  create_interval_graph2();
      
});