/*
  
  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

dstApp.controller('graph1Controller', function($scope, $location, socket, server, userInfo, $route) {
    console.log("started graph1");
    
    var startDateTimeStr = userInfo.sessionData.graph1.start.format("DD/MM/YYYY HH:mm:ss");
    var stopDateTimeStr = userInfo.sessionData.graph1.stop.format("DD/MM/YYYY HH:mm:ss");   
   $scope.rangeStamp = startDateTimeStr+ " - " +  stopDateTimeStr;    
   
   $scope.graph1_interval = userInfo.sessionData.graph1.interval_mode;

   var global_query = {from: userInfo.sessionData.graph1.start, to: userInfo.sessionData.graph1.stop, interval_mode: userInfo.sessionData.graph1.interval_mode};
    /*
        Controller logics and scope variables
    */
    var intervalID = 0, led_intervalID= 0;

    
    $scope.autosync = 1;    
    $scope.graph1_timer_value = 5; 

    $scope.change_graph1_interval = function(){
        console.log("value of inerval changed and is "+ $scope.graph1_interval_+ " and saved");
        userInfo.sessionData.graph1.interval_mode = $scope.graph1_interval;

        //updating in globalvariable of this controller
        global_query.interval_mode = $scope.graph1_interval;

        syncAll(global_query);
    };
        
    $scope.change_graph1_timer = function()
    {      
      $scope.autosync = 0;
      
      if($scope.graph1_timer_value % 1 === 0)
      {
         console.log("value of scope variable is "+ $scope.graph1_timer_value);      
      } 
      else
      {
        $scope.graph1_timer_value = 5;
        console.log("enter a valid value");

      }
      $scope.graph1_timer();
      $scope.autosync = 1;

    }

    $scope.graph1_timer = function(){
      console.log("checkbox clicked and value is "+ $scope.autosync+ "and check button value is"+ $scope.check_button);

      if($scope.autosync == 1)
      {
        var n1 =  $('#noty_area').noty({ dismissQueue: true, maxVisible: 50,  maxVisible: 50, timeout: 5000, template: '<span class="notification n-attention">Choosing autosync can drain lot of data if you are running on 3G.</span>'});
          intervalID = setInterval(function(){           
            
            syncAll(global_query);

        }, $scope.graph1_timer_value*1000);   

            led_intervalID = setInterval(function(){
              $('.led1').toggleClass('led-green');              
          }, 500);   
      }  
      else
      {
         clearInterval(intervalID);
          clearInterval(led_intervalID);
      } 
    }

    $scope.$on('$destroy', function (event) {
      
      console.log("leaving graph1 controller and all intervals removed");          
      $scope.autosync = 0;
       $scope.graph1_timer();

      
      console.log("removed all");
      // or something like
      // socket.removeListener(this);
    });


    /*
    $scope.graph_update = function()
    {
        chart1.xAxis[0].isDirty = true;
        chart1.redraw();
    }
    
    
    */
    var sync_redraw  = function(new_data){
        //newdata.series and new_data.category

        //chart1.series[1].data[0].y is to be changed and chart is redrawn

    };

    var syncAll = function(info){
        //sync and call draw function in call back
        //ask data and call on create graphh on  updated data
        server.get_series1(info, function(new_data){     //new_data is processed data comming from server                    
                        
            console.log("got reply from the callback and calculating for the graph");                    
            create_graph(new_data.series, new_data.cat);                      
        });
    };

    var create_graph = function(series_in, categoris_ar) {
        //console.log("i am inside graph and value of seriesin is"+ series_in);
        //calculate the values and interval for graph with helper

        //calculating whether monthly or daily...   by hash map
        var type = ["Hourly", "Daily", "Weekly", "Monthly", "Yearly"];    //0-1-2-3-4

        var $container = $("#container");        
        chart1 = new Highcharts.Chart({
            chart: {
                type: 'column',
                renderTo: 'container',
                animation: {
                 duration: 1500
             }
         },
         title: {
            text: type[$scope.graph1_interval*1] + ' Occurrence of events'
        },
        subtitle: {
            text: "User: "+userInfo.name+" ("+moment(userInfo.sessionData.graph2.start).format("DD/MM/YYYY HH:mm:ss")+" to "+ moment(userInfo.sessionData.graph2.stop).format("DD/MM/YYYY HH:mm:ss")+ " )"
        },
        xAxis: {
            categories: categoris_ar
            //categories: ['Jan','Feb','March','April', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec','d', 'Oct', 'Nov', 'Dec','d']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Frequency (times)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',

            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.0f} times</b></td></tr>',

            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        credits:{
            enabled: false
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },

        series: series_in

        /*series: [{
            name: 'Television',
            visible: true,
            data: [49.9, 71.5, 6.4, 29.2, 44.0, 76.0, 35, 14.5, 26.4, 14.1, 95.6, 5.4,1,2,3,4,0, 9]

        }, {
            name: 'Cooking',
            data: [3.6, 7.8, 8.5, 9.4, 16.0, 4.5, 15.0, 14.3, 91.2, 23.5, 16.6, 52.3]

        }, {
            name: 'Drinking Coffee',
            data: [100.9, 138.8, 139.3, 141.4, 147.0, 148.3, 159.0, 159.6, 152.4, 165.2, 159.3, 151.2]

        }, {
            name: 'Computer',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

        }]*/

        });
    };


    //create_graph();
    syncAll(global_query);
    var create_interval_graph1 = function()
    {
        $('#range_graph1').daterangepicker(
        {
            timePicker: true,
            timePickerIncrement: 1,
            format: 'DD/MM/YYYY HH:mm:ss', 
            pick12HourFormat: false,        
            //06/19/2014 22:00 - 06/20/2014 22:00
            startDate: userInfo.sessionData.graph1.start,//startDateTimeStr,//'19/06/2014 22:00:00',
            endDate: userInfo.sessionData.graph1.stop//stopDateTimeStr//'20/06/2014 22:00:00'
        }, 
        function(start, end, label) {
            console.log("CALLED"); 
            
            console.log(start.date()+"-"+(start.month()+1)+"-"+start.year()+"--"+start.hour()+":"+start.minute());
            userInfo.sessionData.graph1.start = this.startDate;
            userInfo.sessionData.graph1.stop = this.endDate;                    

            console.log("value saved is "+ userInfo.sessionData.graph1.start+ " and end time is "+ userInfo.sessionData.graph1.stop);
            //chart2.xAxis[0].setExtremes(start+2*3600000, end+2*3600000);
            //start.zone(0);
            //end.zone(0);
            /*
            console.log("after changing zone to 0");
            console.log("timestamp start is"+ start);
            console.log("timestamp end is"+ end);*/

            global_query.from = this.startDate;
            global_query.to = this.endDate;             
            
            syncAll(global_query);
            //call redraw here with new value
            
       });
    };
    create_interval_graph1();
});