	/*
  
  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */
dstApp.controller('livestreamController', function($scope, $location, socket, userInfo) {


	console.log("started live controller");
	//ui,later to be rewrite
	(function(){
    $( "#radioset" ).buttonset();      
    $( "#fullscreentoggle" ).button();
       console.log("calledtoggle");
  	})();


  	socket = socket.getSocket();
  	$scope.clientid = "";
    $scope.power = 1;
    $scope.cameraId = "kinect";
  	$scope.mode = "Depth"
  	$scope.imagerec = "/images/page_images/dst.png";
    //global
    var ImageElm_rgb =  document.getElementById ("imgelem_rgb");

  	$scope.switchcam = function(val)
  	{
  	  socket.emit("command", val);
  	  
  	  switch(val)
  	  {
  	    case 10: $scope.mode = "Depth";
  	    break;
  	    case 11: $scope.mode = "RGB";
  	    break;
  	    case 12: $scope.mode = "IR";
  	    break;
  	  }
  	}  

    $scope.switchPower = function()
    {
      /*var val = 0;
      switch($scope.power)
      {
        case 10: val = "";
        break;
        case 1000: val = "RGB";
        break;        
      }*/
      socket.emit("command", $scope.power);
            
      if($scope.power==0)
      {
        setTimeout(function() {
          ImageElm_rgb.src =  "/images/page_images/dst.png";
        }, 400);
        
      }
      
    }      

  	$scope.changeDevice = function()
  	{  	      	   
  	    socket.emit('register', $scope.clientid);

  	    console.log("updated with client id: "+$scope.clientid);
  	}

  	 	
  	var message = function(msg) {
  		$scope.remotemessage = msg;  	  
  	}



  	var image_rgb = function(base64Image) { 
    
  	   ImageElm_rgb.src =  'data: image / jpeg; base64,'  +  base64Image;  	  
      //$scope.imagerec =  'data: image / jpeg; base64,'  +  base64Image;
  	}

  	socket.on('connect', function () {
  	  console.log(" you are connected");  	  
  	});
  	socket.on('user message', message);
  	socket.on('image_rgb', image_rgb);

	$scope.$on('$destroy', function (event) {
		console.log("leaving livestream controller and all socket removed");  		  	
  		socket.emit('deregister', $scope.clientid);

  		socket.removeAllListeners();
  		console.log("removed all");
  		// or something like
  		// socket.removeListener(this);
  	});


  	

});