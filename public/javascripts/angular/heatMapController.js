/*
  Function: initiate all operation for heat map
  Interacts with DB: Yes
  Entry point: Launch()

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com

*/


dstApp.controller('heatMapController', function($scope, $location, socket, userInfo, server, $route, $http) {

  //var chart;
   
  var startDateTimeStr = userInfo.sessionData.heatMap.start.format("DD/MM/YYYY HH:mm:ss");
  var stopDateTimeStr = userInfo.sessionData.heatMap.stop.format("DD/MM/YYYY HH:mm:ss");
  $scope.rangeStamp = startDateTimeStr+ " - " +  stopDateTimeStr;    

  //$scope.eventsList = [];
  $scope.currentEvent = -1;
  $scope.status = "Please wait.......";
  /*
   Generation
  /////////////////////////////////////////////////////////////////////////*/
  //////////////////////////////////////////////////////////////////////////////
  /*
  Controller logics and scope variables
  */
  var intervalID = 0, led_intervalID= 0;
  $scope.autosync = 1;    
  $scope.heatMap_timer_value = 5;
    
  $scope.change_heatMap_timer = function()
  {      
    
    
    $scope.autosync = 0;
    
    if($scope.heatMap_timer_value % 1 === 0){
      console.log("value of scope variable is "+ $scope.heatMap_timer_value);      
    } 

    else{
      $scope.heatMap_timer_value = 5;
      console.log("enter a valid value");
    }
    
    $scope.heatMap_timer();
    $scope.autosync = 1;


  }

  $scope.heatMap_timer = function(){
  
    console.log("checkbox clicked and value is "+ $scope.autosync+ "and check button value is"+ $scope.check_button);
    if($scope.autosync == 1)
    {
      var n1 =  $('#noty_area').noty({ dismissQueue: true, maxVisible: 50,  maxVisible: 50, timeout: 5000, template: '<span class="notification n-attention">Choosing autosync can drain lot of data if you are running on 3G.</span>'});
      intervalID = setInterval(function(){           
      //launch();
      }, $scope.heatMap_timer_value*1000);   
      led_intervalID = setInterval(function(){
        $('.led1').toggleClass('led-green');              
        }, 500);   
    }  
    else
    {
        clearInterval(intervalID);
        clearInterval(led_intervalID);
    } 
  }

  var updateComboBox = function(eventsArray){
     $scope.status = "StandBy";
      $scope.eventsList = eventsArray;
      console.log("updating the view with all events");
  };

  $scope.changeCurrentEvent = function(){
    
    console.log("changed to "+ $scope.currentEvent);
    //swichPlots($scope.currentEvent, 10, 500000000000000);
    swichPlots($scope.currentEvent, userInfo.sessionData.heatMap.start, userInfo.sessionData.heatMap.stop);

    
  };

  $scope.$on('$destroy', function (event) {
    console.log("leaving heatMap controller and all intervals removed");          
    $scope.autosync = 0;
    $scope.heatMap_timer();  
    console.log("removed all");
    // or something like
    // socket.removeListener(this);
  });

  ///////////////////////////////////////////////Main logic
  /*
    Launch to get  data sync and draw graph
   
  */

  $scope.currentEvent = "-1";

  var scaleFactor = 1;
  var xMin = 1, yMin =1;
  var xMax = 1, yMax = 1;
  var canvas;

  var heatmap;
  var markObjects;

  var roomTexts;



  var processData = function(data){

    globalMinima = -1;
    xMax = 10, xMin = -1;  
    yMax = 10, yMin = -1;  
      
     
    for(var i = 0; i<data.length; i++){
        for(var j =0 ; j<data[i].points.length; j++){        
           //x minima maxima
           if(data[i].points[j].x < xMin){
              xMin = data[i].points[j].x;
            } 

            if(data[i].points[j].x > xMax){
              xMax = data[i].points[j].x;
            }      

            //y minima  maxima
            if(data[i].points[j].y < yMin){
              yMin = data[i].points[j].y;
            }         

            if(data[i].points[j].y > yMax){
              yMax = data[i].points[j].y;
            }      

          }
      }  
      
      console.log("X ranges from "+ xMin+ " to "+ xMax);
      console.log("Y ranges from "+ yMin+ " to "+ yMax);  

      var realWidth = (xMax-xMin), realHeight = (yMax-yMin);

      console.log("width and height in real world ="+realWidth + " , "+ realHeight);
      console.log("width and height of canvas is "+ canvas.width +" , "+ canvas.height);
      
      var xScale = (canvas.width/realWidth), yScale = (canvas.height/realHeight);

      console.log("Scale factor for canvas/real is => "+ xScale +" , "+ yScale);

      

      scaleFactor = xScale < yScale ? xScale : yScale;

      console.log("final scale factor  chosen is "+ scaleFactor);

      // changing data for the base as 0 PROCESSING DATA
      for(var i = 0; i<data.length; i++){      
          var localminX = 5000, localminY = 5000;
          var localmaxX = -1, localmaxY = -1;

          for(var j =0; j<data[i].points.length; j++){
              //console.log("value of x = "+ data[i].points[j].x + " and y = "+ data[i].points[j].y);
              data[i].points[j].x = (data[i].points[j].x - xMin)*scaleFactor;
              data[i].points[j].y = (data[i].points[j].y - yMin)*scaleFactor;                  

              data[i].points[j].x = Math.round(data[i].points[j].x);
              data[i].points[j].y = Math.round(data[i].points[j].y);
              data[i].points[j].y = canvas.height - data[i].points[j].y;  

              //x Local minima maxima
              if(data[i].points[j].x < localminX){
                localminX = data[i].points[j].x;
              } 

              if(data[i].points[j].x > localmaxX){
                localmaxX = data[i].points[j].x;
              }      


              //y local minima  maxima
              if(data[i].points[j].y < localminY){
                localminY = data[i].points[j].y;
              } 

              if(data[i].points[j].y > localmaxY){
                localmaxY = data[i].points[j].y;
              }                
          }
          data[i].center.x = (localmaxX + localminX)/2;
          data[i].center.y = (localmaxY + localminY)/2;

          console.log("local minima is "+ localminX+" , "+ localminY);
          console.log("local maxima is "+ localmaxX+" , "+ localmaxY);      
          console.log("center of the shape at index "+ i+" is"+data[i].center.x+" , "+ data[i].center.y  );

      }


      return data;

    }


    var fetchObjects = function(callback){
       canvas = new fabric.Canvas('c1');
      //raw data from ajax call and their location
      //defalt colur #0E0C0C
      
    /*  var rawdata = [ {name: "Bathroom", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: -1580},
                                                                  {x: -2000, y: -1580},
                                                                  {x: -2000, y: -3450},
                                                                  {x: 0, y: -3450}]  },

                    {name: "Bedroom", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0},
                                                                  {x: -2000, y: 0},
                                                                  {x: -2000, y: 3370}, 
                                                                  {x: 0, y: 3370}]  },

                    {name: "Corridor", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                                   {x: -2000, y: 0}, 
                                                                   {x: -2000, y: -1580}, 
                                                                   {x: 0, y: -1580}]  },

                    {name: "Kitchen", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                                  {x: 2980, y: 0}, 
                                                                  {x: 2980, y: -3450}, 
                                                                  {x: 0, y: -3450}]  },

                    {name: "Living room", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                                      {x: 2980, y: 0}, 
                                                                      {x: 2980, y: 3370}, 
                                                                      {x: 0, y: 3370}]  } ];  
                                                                      */
      server.get_heatMap(function(){
	
	console.log("data is before processing");
	console.log(userInfo.sessionData.heatMap.data.design);
	var data = processData(userInfo.sessionData.heatMap.data.design);   
	
	  //var data = processData(rawdata);   
	//i start from here.

	for(var i =0; i<data.length; i++){
	  for (var j = 0; j< data[i].points.length; j++){ 
	    //console.log("value of x = "+ data[i].points[j].x); 
	   // console.log("   value of y = "+ data[i].points[j].y); 
	  }
	}      
	roomTexts = new Array();  
	var shape_grp = new fabric.Group([], { id: "rooms", left: 0, top: 0, scaleX: 1, scaleY: 1});                
	for(var i =0; i<data.length; i++){
	  var coordinates = data[i].points;
	  var colour = data[i].colour;	
	  var center = data[i].center;
	  //also add text here      
	  var text = new fabric.Text(data[i].name, { id: "text", left: center.x, top: center.y, originX: 'center', originY: 'center', fontSize: 20,fill: 'black',opacity: 1}); 
	  roomTexts.push(text);
	  //text array updated
	  var str = getPathString(data[i].points);
	  //console.log(" value of string AT INDEX "+i+" is "+str+"\n");                                                                  
	  var shape = new fabric.Path(str, {  originX: 'center', originY: 'center', opacity: 0.7, fill: colour, strokeWidth: 1, stroke: 'rgba(0,0,0,1)'}); 
	  shape_grp.add(shape);  
	}
	//var bedroomS = new fabric.Path('M  0 0 L 0 200 L 200 200 L 200 0 z', {  originX: 'center', originY: 'center', opacity: 0.5, fill: 'red'});                 
	//var badS = new fabric.Path('M  0 200 L 200 200 L 200 300 L 0 300 z', {  originX: 'center', originY: 'center', opacity: 1, fill: 'yellow'});                     
	callback(shape_grp, roomTexts);		
      });									               
    }


    /**
    * helper function to get the 
    *
    **/

    var getPathString = function(points){
      
      var str = "M"
      for(var i = 0;; i++){
        
        if( i== points.length){
          str = str + " z";    
          break;
        }

        str = str + " "+points[i].x +" " +points[i].y;
        if((i+1) !== points.length){
          str= str + " L"
        }


      }  
      return str;
    }

    /**
    *   ENTERY POINT FOR Background drawing
    *
    */
    var createMap   = function () {
      fetchObjects(function(rooms, texts){
        // adding rooms to canvas
        canvas.add(rooms);
        //text coordinate are calculated seperately
        for (var i = 0; i < texts.length; i++) {
          canvas.add(texts[i]);
        };  
        //canvas.add(textb);        
        //canvas.add(textbad);        
        canvas.renderAll();        
      });      
               
    }
    
    
    $scope.nameVisible = true;
    $scope.toggleNames = function(){
      if($scope.nameVisible)
        switchNames(0);
      else
        switchNames(1);
    }
    var switchNames = function(enable){

      if(enable==0){
        for(var i = 0; i < roomTexts.length; i++){
          if(canvas.contains(roomTexts[i])){
            canvas.remove(roomTexts[i]);          
          }
          else{
            console.log("No element on sreen to delete.");
            return;
          }              
        }        
      }
      //in case to display it on screen.
      else{
        for(var i = 0; i < roomTexts.length; i++){
          
          if(!canvas.contains(roomTexts[i])){
            canvas.add(roomTexts[i]);          
          }
          else{
            console.log("Already there");
            return;
          }              
        }    
      }
      setTimeout(function(){canvas.renderAll();}, 100);  
    }
      


    /*
    **  Helper function for the Heat maps
    */

    var updateLegend = function (data) {

      var legendCanvas = document.createElement('canvas');
      legendCanvas.width = 100;
      legendCanvas.height = 10;

      var legendCtx = legendCanvas.getContext('2d');
      var gradientCfg = {};
      function $(id) {
        return document.getElementById(id);
      };

      // the onExtremaChange callback gives us min, max, and the gradientConfig
      // so we can update the legend
      $('min').innerHTML = data.min;
      $('max').innerHTML = data.max;
      //$('min').innerHTML = "1";
      //$('max').innerHTML = "10";

      // regenerate gradient image
      if (data.gradient != gradientCfg) {
        gradientCfg = data.gradient;
        var gradient = legendCtx.createLinearGradient(0, 0, 100, 1);
        for (var key in gradientCfg) {
          gradient.addColorStop(key, gradientCfg[key]);
        }

        legendCtx.fillStyle = gradient;
        legendCtx.fillRect(0, 0, 100, 10);
        $('gradient').src = legendCanvas.toDataURL();
      }
    };

    var createHeatMap = function(){

        heatmap = h337.create({
        container: document.getElementById("heatmap"),
        maxOpacity: .7,
        radius: 500*scaleFactor,
        blur: 0.85,
        // update the legend whenever there's an extrema change
        onExtremaChange: function onExtremaChange(data) {
          updateLegend(data);
        }
      });

    };

     
    var fetchObjects_heatData = function(callback){

      //camefrom database
     /* var raw_data = [{event_id: 2, event_name: "Sitting", points:[  { x: 2154, y: 14, time: 5}, {x: 2189, y: 503, time: 5000000000000000}, {x: 126, y: 2239, time: 50},{x: 126, y: 2239, time: 5000000000000000}, {x: -653, y: 355, time: 5000000000000000}]},
                      {event_id: 3, event_name: "Sleeping", points:[{x: 154, y: 414}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                      {event_id: 4, event_name: "Lying", points:[{x: 2154, y: 14}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                      {event_id: 9, event_name: "Drinking", points:[{x: 2154, y: 214}, {x: 2189, y: 93}, {x: 326, y: 229}, {x: -63, y: 355}]},
                      ];
       */ 
      //processed
      server.get_heatMapPoints(function(resp){
       var raw_data = resp.series;       
	   for(var i = 0; i< raw_data.length; i++){  
	     for(var j = 0; j< raw_data[i].points.length; j++){
	       raw_data[i].points[j].x =  Math.round((raw_data[i].points[j].x - xMin)*scaleFactor);      
	       raw_data[i].points[j].y =  Math.round((raw_data[i].points[j].y - yMin)*scaleFactor);
	       //as in web its origin is different
	       raw_data[i].points[j].y = canvas.height - raw_data[i].points[j].y;
	       }        
	   }
	   markObjects = raw_data;
	   callback(markObjects, resp.eventsList);                  					
    });			            
    };

    /**
    * data here is only row of a single event id which is to be  displayed currently after time restriction
    */
    var applyTime = function(data, startStamp, endStamp){

      var timmedData = JSON.parse(JSON.stringify(data));
      timmedData.points = new Array();
      for(var j = 0; j< data.points.length; j++){
          if(data.points[j].time >= startStamp && data.points[j].time < endStamp){
            console.log("true");
            timmedData.points.push({x: data.points[j].x, y: data.points[j].y, value: 1} );
          }
      }  

      return timmedData;

    }


    /**
    * Object contains array of array 
    *
    */
    var swichPlots = function(event_id, start, end){
        
      var dataToMap = applyTime(markObjects[event_id], start, end);

      console.log("initial value is "+ dataToMap.points.length);  
      console.log("value of data to map is ");
      
      for(var j = 0; j< dataToMap.points.length; j++){
	console.log("x = "+ dataToMap.points[j].x+" , y = "+ + dataToMap.points[j].y );
      }
      
      heatmap.setData({    
        max: dataToMap.points.length,
        min:0,
        data: dataToMap.points
        //data: [{ x: 0, y: 0, value: 1}, { x: 100, y: 25, value: 1}, { x: 100, y: 25, value: 1}, { x: 110, y: 25, value: 4}, { x: 120, y: 25, value: 1}, { x: 100, y: 225, value: 1}]
      });

     
      var maxV = -1, minV = 100000;
      console.log("value of loop vars are "+ canvas.width + ", "+canvas.height);
      for (var xx = 0; xx < canvas.width; xx+=10) {
        for (var yy = 0;yy < canvas.height; yy+=10) {
          var val = heatmap.getValueAt({x: xx,y: yy});          
          if (val > maxV){
            maxV = val;
          } 
          if(val < minV){
            minV = val;
          }
        }
      }

     
      console.log("max value is "+ maxV);
      heatmap.setData({    
        max: maxV,
        min: minV,
        data: dataToMap.points
        //data: [{ x: 0, y: 0, value: 1}, { x: 100, y: 25, value: 1}, { x: 100, y: 25, value: 1}, { x: 110, y: 25, value: 4}, { x: 120, y: 25, value: 1}, { x: 100, y: 225, value: 1}]
      });
    
      console.log("value of TEST: "+ heatmap.getValueAt({x: (2154-xMin)*scaleFactor, y: (14-yMin)*scaleFactor}) );
      
    }

    var generate = function(event_id, start, end) {

      fetchObjects_heatData(function(data, events_for_gui){
        console.log("Map is ready with data processed");
        // push all events in combobox tag via ng        
         console.log("First event in array is  ->"+ events_for_gui[0]); 
         updateComboBox(events_for_gui);


        //checking data after all processing
        for(var i =0; i<data.length; i++){
           for (var j = 0; j< data[i].points.length; j++){ 
            //console.log("value of x = "+ data[i].points[j].x); 
            //console.log("   value of y = "+ data[i].points[j].y); 
          }
        }
        //swichPlots(3, 10, 500000000000000);
			  //1411997827265

      });
    }
   

     /* First time draw, not too early ;)*/
   $(document).ready(function(){      

      createMap();
      setTimeout(function(){    
        createHeatMap();
        generate();

      }, 1000); 

       //timer("set", 10000);
      // $scope.timer();
   }); //doc ready
  


///////////////////////////////////////////////////////////////////////////////////
  var create_interval_heatMap = function()
  {
      
      $('#range_heatMap').daterangepicker(
      {
          timePicker: true,
          timePickerIncrement: 1,          
          format: 'DD/MM/YYYY HH:mm:ss',    
          pick12HourFormat: false,        
          //06/19/2014 22:00 - 06/20/2014 22:00
          startDate: userInfo.sessionData.heatMap.start,//startDateTimeStr,//'19/06/2014 22:00:00',
          endDate: userInfo.sessionData.heatMap.stop//stopDateTimeStr//'20/06/2014 22:00:00'
      }, 
      function(start, end, label) {
         console.log("CALLED"); 
         
         console.log(start.date()+"-"+(start.month()+1)+"-"+start.year()+"--"+start.hour()+":"+start.minute());


        console.log("end time is "+ end.date()+"-"+(end.month()+1)+"-"+end.year()+"--"+end.hour()+":"+end.minute());         

         

         //console.log("value saved is "+ userInfo.sessionData.heatMap.start+ " and end time is "+ userInfo.sessionData.heatMap.stop);

         //chart2.xAxis[0].setExtremes(start+2*3600000, end);

         start.zone(0);
         end.zone(0);
         console.log("after changing zone to 0");
         console.log("timestamp start is"+ start);
         console.log("timestamp end is"+ end);
         userInfo.sessionData.heatMap.start = start;
         userInfo.sessionData.heatMap.stop = end;
         swichPlots($scope.currentEvent, start, end);
         //chart2.xAxis[0].setExtremes(start, end);
     });
  };
  /*   Don't forget me, i am also important :)*/
  create_interval_heatMap();
      
});