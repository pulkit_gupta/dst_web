	<!--
 /*
  Function: Main Includes controller and file ffor angular app
  Interacts with DB: No
  Entry point: controllers

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

-->
	// create the module and name it dstApp
	var dstApp = angular.module('dstApp', ['ngRoute', 'ngAnimate', 'FBAngular']);

	// configure our routes
	dstApp.config(function($routeProvider) {
		$routeProvider

			// route for the home page
			.when('/', {
				templateUrl : 'pages/home.html',
				controller  : 'mainController'
			})

			.when('/login', {  //currently the starting point
				templateUrl : 'pages/login.html',
				controller  : 'loginController'
			})

			
			.when('/member', {   //main page entery
				templateUrl : 'pages/admin.html',
				controller  : 'memberController'
			})

			// route for the contact page
			.when('/contact', {
				templateUrl : 'pages/contact.html',
				controller  : 'contactController'
			});
	});

	// create the controller and inject Angular's $scope
	dstApp.controller('mainController', function($scope, $location, userInfo) {
		// create a message to display in our view
		$scope.pageClass = '';
		$scope.message = 'Will be uploaded soon!';
		$scope.session = userInfo.session; 
		$location.path( "/login" );		
	});
	

	dstApp.controller('contactController', function($scope) {
		$scope.message = 'Contact us! JK. This is just a demo.';
		$scope.pageClass = 'page-contact';
	});

	dstApp.controller('aboutController', function($scope) {
		$scope.profile = "i am in about controller"; 
	});
