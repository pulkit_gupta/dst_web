/*
  
  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

dstApp.controller('loginController', function($scope, $location, $http, userInfo) {
		// create a message to display in our view
		$scope.pageClass = '';
		$scope.message = '';
		$scope.title = "logindst";
		$scope.doLogin = function() {     
		        var req = { "username": $scope.username, "password": $scope.password };
		        $http({ method: "POST", url: "/login", data: req }).success(function (resp) {
		        	if(resp.error === "wrong")
		        	{
		        		console.log("authentication failed due to wrong credentials");
		        	}
		        	else if(resp.token)
		        	{
		               	userInfo.token = resp.token; 
		               	console.log("token saved in user repo is "+userInfo.token);		
		            	console.log(" got the token :)");
		            	userInfo.session = true;		            
		            	$location.path( "/member" );
		            }
		        });	
		};

		$scope.samplelogin = function()
		{
			var req = { "username": "pulkit", "password": "dst"};
			$http({ method: "POST", url: "/login", data: req }).success(function (resp) {
				if(resp.error === "wrong")
				{
					console.log("authentication failed due to wrong credentials");
				}
				else if(resp.token)
				{
			       	userInfo.token = resp.token; 
			       	console.log("token saved in user repo is "+userInfo.token);		
			    	console.log(" got the token :)");
			    	userInfo.session = true;		            
			    	$location.path( "/member" );
			    }
			});	
		};
		
	});
