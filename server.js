 /*
  Function: Serves DST app 
  Interacts with DB: Yes
  Entry point: main http server call
  Configuration file: config.js

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */


var api = require('./routes/api');

var g1_api = require('./routes/graph1API');
var g2_api = require('./routes/graph2API');
var g3_api = require('./routes/graph3API');
var heat_api = require('./routes/heatMapAPI');

var express = require('express');
var http = require('http');

var socketIo = require('socket.io');
var socketio_jwt = require('./node_modules/socketio_jwt'); //require('socketio-jwt');

var zmq = require('zmq');
var subscriber = zmq.socket('sub');
var publisher = zmq.socket('pub');
mysql = require('mysql');
var moment = require('moment');

/* GLOBAL VARIABLE */
connection = ""; 

jwt = require('jsonwebtoken');  //optionally used for ajax
jwt_secret = 'Pulkit, you are my secret client ;)';
online_user = Array();  
config = require('./conf').Config





//connect my sql
reconnect = function(db_name){
  console.log('Trying to connect sql. in server.js');
  connection = mysql.createConnection({
    host     : "localhost",
    user     : config.db_user,
      password : config.db_password,
      database : db_name? db_name : config.db_name
    });   
    connection.connect(function(err)
  {
    if(err)
    {
      console.log("failed and reconnecting with db name "+ dbName);

      reconnect(dbName);
    }
    else
    {
      console.log("connected");
      connection.on('error', function(err) 
      {
         /* if (!err.fatal) {
            return;
          }*/
        if (err.code !== 'PROTOCOL_connection.eventDB_LOST') {
            //throw err;
            console.log("Error is continous. and can not be solved.");
          }
          else
          {
            console.log("Retrying!!unexpected connection.eventDB lost.");
            reconnect();
          }    
      });

    }

  });     
}
/* Global var and functions end*/
var filter = "";
subscriber.subscribe(filter);
var app = express();

/*Global variable */



app.configure(function(){
  this.use(express.json());
  //this.use(express.bodyParser());
  this.use(express.json());
  this.use(express.urlencoded());
  this.use(express.static(__dirname + '/public'));
});

/* FORMAT var userData = {name: "", remoteDB_conn: "", fid: "", remoteInfo:{ event_data:[], event_name_array: [], num_of_events: ""  }, series1: "", series2: "", series3: "", lastupdated: ""};*/

/* Http request*/
app.post('/login', api.login); 

app.post('/getHeatMap', heat_api.getHeatMap);

app.post('/getHeatPoints', heat_api.getHeatPoints);

app.post('/getDataG3', g3_api.getDataG3);
app.post('/getDataG2', g2_api.getDataG2);
app.post('/getDataG1', g1_api.getDataG1);


/* socket code starts*/
var sockets_live = new Array();
var server = http.createServer(app);
var sio = socketIo.listen(server);

sio.set('authorization', socketio_jwt.authorize({
  secret: jwt_secret,
  handshake: true
}));

sio.sockets.on('connection', function (socket) {
     
    socket.emit('user message', " server says: hello, You are connected");      
    var image = 0;
    //socket.emit('user image', image);  

    socket.on('register', function(name){
   
      //mapping of socket: array["socket name"] = object
        if(sockets_live[socket.name]!= undefined)
        {
          sockets_live[socket.name]= undefined;  //deleting old registeration, because no two names can have socket at the same time
        }
        socket.name = name;
        sockets_live[name] = socket;
        //console.log(name+ " is added/registered in list as object = "+ sockets_live[name].id);
        sockets_live[name].emit('user message', "Now you are registered and confirmed as "+name);
    });

    
    socket.on('deregister', function(){
        console.log("deregisterying "+ socket.name);
        if(sockets_live[socket.name]!= undefined)
        {
          sockets_live[socket.name]= undefined;  //deleting old registeration, because no two names can have socket at the same time
        }            
    });
    
        

    socket.on('disconnect', function (){
      console.log("CLIENT disconnect");
      console.log("spliced the socket object");

    });  
    
    socket.on('command', function(val){    
      console.log("client asked for camera");
      var strvalue = socket.name+"."+val+".";
      publisher.send(strvalue); 
      console.log(strvalue);      
    });
    
  });

 
//starts server
server.listen(config.port, function () {
  console.log('listening on '+config.ip+ ':'+config.port);
  console.log("running with domain name:"+ config.appURL);
  console.log("DB name:"+ config.db_name);
  reconnect(config.db_name);

});