<!--
 /*
  Function: Main give all configurations
  Interacts with DB: No
  Entry point: development

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

-->
var development = {
  
  appURL : 'localhost',
  
  timeZone: 5.5,
  
  zmq_r: "tcp://134.109.5.21:9001",
  zmq_t: "tcp://134.109.5.21:9002",
  
  mailKey: "7c745ce4",
  siteEmail: "info@dst.de",
  port : 9000,

  /* DB settings */
  ip:"localhost ",   //localhost  
  db_user     : 'root',
  db_password : 'pulkit',
  db_name : 'dst',

  env : global.process.env.NODE_ENV || 'development'
};









var production = {
};

exports.Config = global.process.env.NODE_ENV === 'production' ? production : development;