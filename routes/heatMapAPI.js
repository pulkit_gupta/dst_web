/*
 Function: Serves DST main server file with APIs post requests of heatMap 
 Interacts with DB: Yes (moderate)
 Entry point: getDataHeat
 Configuration file: config.js
 Sharing: Global variables in server.js and api.js
 
 Author: Pulkit Gupta
 Query email: pulkit.itp@gmail.com
*/



exports.getHeatMap = function (req, res) {
    //value from database will be fetched later on 
  var userData = getObject(req.body.token);

  if( userData.session === true){
      //get value stored earlier      
      if(!userData.DB.db_rooms.object){
	       console.log("no connection to database, please check");		
	       reconnect_All_RemoteDB(token);	
      }      
      sync_design_DB(userData.DB.db_rooms.object, function(dataObj){      	     	
	       userData.heatMapDeisgn = make_design(dataObj);                        		      
	       if(updateObject(userData, req.body.token)){
	           console.log("updated the object with data of series 2");
	       }      
	       console.log("sent data for deisgn");
	       console.log("data of the points are like this"+ userData.heatMapData);
	       res.json({design: userData.heatMapDeisgn});  			
      });                        
  }

  else
  {    // No session, only for Hackers
    res.json({data_error: "wrong"});
    console.log("Data json not sent instead a warning is sent"); 
  }

};
			
/**
 *	for design purpose
 *
 *
 **/	

var getPointsArray = function(dataRow){
  var pointArray = new Array();
  
  for(var j = 0; j<dataRow.anz_corner; j++){
    
    pointArray.push({x: dataRow["corner"+j+"_x"], y: dataRow["corner"+j+"_y"] });    
  }
  return pointArray;
};

var make_design = function(data){
  var result= new Array();
  for(var i = 0; i< data.length; i++){
    result.push({name: data[i].name, colour: "black", center: {x: "", y: ""}, points: getPointsArray(data[i]) });
  } 
  return result;
}



/**
 *	for the heat points
 * 
 * 
 */

exports.getHeatPoints = function (req, res) {
    //value from database will be fetched later on 
  var userData = getObject(req.body.token);

  if( userData.session === true){
      //get value stored earlier      
      if(!userData.DB.db_events.object){
	console.log("no connection to database, please check");		
	reconnect_All_RemoteDB(token);	
      }
            
	sync_event_DB(userData.DB.db_events.object, function(dataObj){            
	
	  userData.heatMapData = make_heatPoints(dataObj);
	  
	  if(updateObject(userData, req.body.token)){
	    console.log("updated the object with data of heat points");
	  }      
	  console.log("sent data for points");
	  console.log("data of the points"+ userData.heatMapData);
	  res.json({series: userData.heatMapData, name_array: dataObj.event_name_array, num_of_events: dataObj.num_of_events});  	
	});	    
                 
  }
  else
  {    // No session, only for Hackers
    res.json({data_error: "wrong"});
    console.log("Data json not sent instead a warning is sent"); 
  }

};



var get_point_array = function(userInfo, id){ 
  var i =0, pointArray = new Array();
  for(i=0;i<userInfo.event_data.length; i++)
  {
    if(userInfo.event_data[i].event_id== id)
  {
      pointArray.push({x: userInfo.event_data[i].x, y: userInfo.event_data[i].y, time: Math.round(userInfo.event_data[i].timestamp_start/1000) });                  
    }
  }
  //console.log("value returned is for event "+userInfo.event_name_array[id] +"is ->");
  //console.log(intvArray);
  return pointArray;
}

var make_heatPoints = function(userInfo, userID){ 
  var heatObj=Array();
  
  //console.log("value of names are");
  //console.log(userInfo.event_name_array);
  var i =0, element=0;

  for(i=0;i<userInfo.num_of_events; i++)   //giving 16 numbers
  {
    heatObj.push({id: userInfo.event_data[i].event_id, name: userInfo.event_name_array[i], points: get_point_array(userInfo, i+1)});
    
    console.log(heatObj[i]);
    console.log("\n");
  }
                  
  return heatObj;
}



/*/////
   var raw_data = [{event_id: 2, event_name: "Sitting", points:[  { x: 2154, y: 14, time: 5}, {x: 2189, y: 503, time: 5000000000000000}, {x: 126, y: 2239, time: 50},{x: 126, y: 2239, time: 5000000000000000}, {x: -653, y: 355, time: 5000000000000000}]},
                      {event_id: 3, event_name: "Sleeping", points:[{x: 154, y: 414}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                      {event_id: 4, event_name: "Lying", points:[{x: 2154, y: 14}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                      {event_id: 9, event_name: "Drinking", points:[{x: 2154, y: 214}, {x: 2189, y: 93}, {x: 326, y: 229}, {x: -63, y: 355}]},
                      ];
    
 */

/*//////////////////////////////////////////////////////////////////////////////


var dummyData = [ {name: "Bathroom", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: -1580},
                                                                  {x: -2000, y: -1580},
                                                                  {x: -2000, y: -3450},
                                                                  {x: 0, y: -3450}]  },

                    {name: "Bedroom", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0},
                                                                  {x: -2000, y: 0},
                                                                  {x: -2000, y: 3370}, 
                                                                  {x: 0, y: 3370}]  },

                    {name: "Corridor", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                                   {x: -2000, y: 0}, 
                                                                   {x: -2000, y: -1580}, 
                                                                   {x: 0, y: -1580}]  },

                    {name: "Kitchen", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                                  {x: 2980, y: 0}, 
                                                                  {x: 2980, y: -3450}, 
                                                                  {x: 0, y: -3450}]  },

                    {name: "Living room", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                                      {x: 2980, y: 0}, 
                                                                      {x: 2980, y: 3370}, 
                                                                      {x: 0, y: 3370}]  } ];  

								      */