/**
*
*
*
*/
var g3_get_intv_array = function(results, location){ 
  
  var i =0, intvArray = new Array();

  for(i=0;i<results.length; i++)
  {
    if(results[i].location == location)
    {
      console.log("Matched "+ location);
      intvArray.push({from: Math.round(results[i].timestamp_start/1000), to: Math.round(results[i].timestamp_end/1000)});                  
      //console.log("added interval"+ intvArray[i].from + " - "+ intvArray[i].to);
    }
  }
  //console.log("value returned is for event "+userInfo.event_name_array[id] +"is ->");
  //console.log(intvArray);
  return intvArray;
}

var Locations = ["Outside", "Bath", "Bedroom", "Living Room", "Kitchen", "Corridor"];
var g3_make_event_obj = function(conn, callback){
  
  

  var sql = "SELECT * FROM `location_data` WHERE 1";
  conn.query(sql, function(err, results) {        
      if(!results[0]){ console.log("not location data found."); }
      else { 
        console.log("HELLOOO found locations as: "+ results[0].location);        


        for(var k =0; k<results.length; k++){
          console.log(results[k].location);
        }           

        console.log("Now calculating for every place an object");        

        var g3Obj=Array();

        for(var i = 0; i<Locations.length; i++){
          console.log("Calculating for place"+ Locations[i]);        
          g3Obj.push({name: Locations[i], intervals: g3_get_intv_array(results, Locations[i]) });

          if(g3Obj[i].intervals[0])
            console.log("Pushed array is "+ g3Obj[i].name+ " and interval is from: "+ g3Obj[i].intervals[0].from+ " to:"+ g3Obj[i].intervals[0].to);
        }   
        //g3Obj.push({name: "pulkit", intervals: [{from: 0, to: 0 }]});
        callback(g3Obj);

      }
    });        
}

exports.getDataG3 = function (req, res) {
    //value from database will be fetched later on 
  var userObject = getObject(req.body.token);

  if( userObject.session === true){
      //get value stored earlier      
    g3_make_event_obj(userObject.DB.db_events.object, function(dataToSend){

        console.log("Data successfully sent and first values are ");
       for(var i =0; i<dataToSend.length; i++){
          console.log(dataToSend[i].name+ " and values are");
          
       }
        res.json(dataToSend);                        
    });      
  
  }
  else
  {    // No session, only for Hackers
    res.json({data_error: "wrong"});
    console.log("Data json not sent instead a warning is sent"); 
  }

};

//1408276800000
//1458384800