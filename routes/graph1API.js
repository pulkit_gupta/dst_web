/*
 Function: Serves DST main server file with APIs post requests of graph1 
 Interacts with DB: Yes (moderate)
 Entry point: getDataG1

 Configuration file: config.js
 Sharing: Global variables in server.js and api.js
 
 Author: Pulkit Gupta
 Query email: pulkit.itp@gmail.com
*/



var moment = require("moment");


exports.getDataG1 = function (req, res){
    console.log("got request for graph1");
    var userData = getObject(req.body.token);
    if( userData.session === true ){
        //get value stored earlier        
          sync_event_DB(userData.DB.db_events.object, function(dataObj){ 

          //updte the database updated timestamp value to show to user here!   
          userData.remoteInfo = dataObj;  //upate data

          
          //processData(dataObj,1403452800000, 1403733599999 , 1, function(processed_data){
            console.log("VALUE OF body recieved are from: "+ moment(req.body.info.from) + " of  "+ moment(req.body.info.to)+ " and interval mode is "+ req.body.info.interval_mode);
            processData(dataObj,moment(req.body.info.from), moment(req.body.info.to) , req.body.info.interval_mode, function(processed_data){
            userData.series1 =  processed_data;
            if(updateObject(userData, req.body.token)){
               console.log("updated the object with data of series 1");
            }       
               //console.log("Data successfully sent and first value is "+userData.series1.series[0].name);
               res.json(userData.series1);  

          });
       
       });        
                   
    }
    else
    {    // No session, only for Hackers
      res.json({data_error: "wrong"});
      console.log("Data json not sent instead a warning is sent"); 
    }

};

var getCountOfEach = function(dataObj, t_min, t_max)
{   
    var data = dataObj.event_data;
    var number_of_events = dataObj.num_of_events;


    var result = new Array();

    for (var j = 0; j < number_of_events; j++) {
        result[j] = 0;
    };

    for(var i = 0;i<data.length; i++)
    {
        if(data[i].timestamp_start/1000>=t_min && data[i].timestamp_end/1000<t_max)
        {
            var index = data[i].event_id-1;
            result[index] = result[index]+1;
            //console.log("incrementing at index:"+ index + "and value is "+ result[index]);
            
        }
    }
    
    console.log("returning first array = "+ result+"\n");
    return result;
}    

var processData = function(dataObj, from, to, interval_mode, callback){    //data with time limits and interval_mode -> 0,1,2,3  -> hourly, daily, weekly, monthly, yearly

    //check the interval for category strings
    var category_arr = new Array();        
    var interval = 3600*24*1000; //initialization
    //var divider = 0;

    switch(interval_mode*1)
    {
        case 0://hourly
            interval = 3600*1000;  //milli seconds in an hour
            break;
        case 1:            
            interval = 3600*24*1000;  //milli seconds in a  DAY
            //daily
            break; 
        case 2: 
            interval = 3600*24*1000*7;  //milli seconds in a  week
            //weekly
            break;
        case 3: //monthly            
            interval = 3600*24*30*1000;  //milli seconds in a  month
            break;
        case 4:   
            interval = 3600*24*30*1000*12;  //milli seconds in a  year
            break;
        default:
            break; 
    }
    var categories = (to-from)/interval;
    
    var min_time = from;

    var series_data= new Array();
    //categories means from to intervals
    console.log("value of catefory is "+ categories);
    for(var i= 0; i <categories;i++)
    {
        var from_stamp = min_time+(i*interval);
        var to_stamp  = min_time+((i+1)*interval);

        var cnt = getCountOfEach(dataObj, from_stamp,to_stamp);
       // console.log("intervals are "+moment(for_stamp).format('MMMM Do YYYY, h:mm:ss a')+" to " + moment(to_stamp).format('MMMM Do YYYY, h:mm:ss a'));
        //fill the category x axis string array
        switch(interval_mode*1)
        {
            case 0://hourly

                category_arr.push(moment(from_stamp).format('MMMM Do, HH:mm ')+" to "+ moment(to_stamp).format('MMMM Do, HH:mm '));
                break;
            case 1:   //daily

                category_arr.push(moment(from_stamp).format('MMMM Do, hh:mm')+" to "+ moment(to_stamp).format('MMMM Do, hh:mm'));
                
                //daily
                break; 
            case 2: 
                category_arr.push(moment(from_stamp).format('MMMM Do, hh:mm')+" to "+ moment(to_stamp).format('MMMM Do, hh:mm'));
                //weekly
                break;
            case 3: //monthly
                
                category_arr.push(moment(from_stamp).format('MMMM Do')+" to "+ moment(to_stamp).format('MMMM Do'));

                break;
            case 4:   
                category_arr.push(moment(from_stamp).format('MMMM YYYY')+" to "+ moment(to_stamp).format('MMMM YYYY'));
                break;
            default:
                break; 
        }
        series_data.push(cnt);
    }

   //choosing right interval string
   
   console.log("category strings are "+ category_arr);


    var finalSeries = Array();
    //var element = {name: data: []}
    
   //preparing for the graph
    for (var j = 0; j< dataObj.num_of_events; j++) {
       var  data1= new Array();
       for(var i = 0; i< series_data.length ; i++){
            data1.push(series_data[i][j]);           
        }
        finalSeries.push(
            {
                name: dataObj.event_name_array[j+1],
                data: data1,
                visible: true
        });
    }
    
    for(var k = 0; k<finalSeries.length; k++)
        console.log("object of series formed is name:"+finalSeries[k].name+" and data: "+ finalSeries[k].visible);
    
    callback ({series: finalSeries, cat: category_arr});
};

 /*ends here*/
