
/*
    Function: Serves DST main server file with APIs post requests 
    Interacts with DB: Yes (Intense interaction)
    Entry point: several
    Configuration file: config.js
    Sharing: Global variables in server.js
 
    Author: Pulkit Gupta
    Query email: pulkit.itp@gmail.com

*/




/* FORMAT var userData = {token: "", name: "",  DB: {eventDB:{host: "", name: "", uid: "", password: "", object:"", lastUpdated: "" }, deisgnDB:{} },
 *			 fid: "", remoteInfo:{ event_data:[], event_name_array: [], num_of_events: ""  }, series1: "", series2: "", series3: "", heatMapDeisgn: "", heatMapData: ""};
 */
getObject = function (token){
	for(i=0;i<online_user.length; i++){
	  if(online_user[i].token == token){
	  	console.log("Found object in live users");
	  	return online_user[i];	  	
	  }
	}
	return false;
}

updateObject = function (newObj, token){
	for(i=0;i<online_user.length; i++){
	  if(online_user[i].token == token){
	    online_user[i] = newObj;
	  	console.log("successfully updated");
	  	return true;
	  	
	  }
	}	
	return false;
}


//Sync remote db
sync_event_DB = function (connection, callback){
  if(!connection)
  {
  	console.log("no object of sql.....In api.js calling to have sql object")
  	//reconnect("dst1");
  }
  var eventsName= Array();
  //connection.eventDB.query("SELECT MAX(event_id) FROM events", function (err, results, fields) {
  connection.query("SELECT event_id FROM events ORDER BY event_id  DESC LIMIT 1", function (err, results, fields) {  
    if (err) {
      throw err;
    }
    var max = results[0].event_id;


    var sql= 'SELECT * FROM events';
    
    console.log("Requesting remote device for complete raw data");
    connection.query(sql, function(err, results) {        
      if(!results[0]){ console.log("not found any event names"); }
      else { 
          console.log("found data as: ");
          for(var i in results){

            /*console.log("Id of event is "+String(results[i].event_id)+ " ");
            console.log("name of event is "+ results[i].event_name+ " ");*/
            eventsName[results[i].event_id] = results[i].event_name;          

            
          }
          console.log(max);
          var sql= "SELECT * FROM events_data ORDER BY  `events_data`.`timestamp_start` ASC "; 
          connection.query(sql, function(err, results) {        
            if(!results[0]){ console.log("not found any data in events_table");}
            else
            {              
              console.log("first element is with fid="+ results[0].fid);
              //console.log(results);
              callback({event_data: results, event_name_array: eventsName, num_of_events: max});
            }   
          }); //inner sql
          
      }   
    }); //outer sql

  });
}


//Sync remote db wnd get the design db and rooms coordinates

sync_design_DB = function (connection, callback){
   
  connection.query("SELECT * FROM room ", function (err, results) {  
      if (err) {
	throw err;
      }    
      else if(results!=null){
	
	console.log("got the data of deisgn from db");
	//console.log(results);
	
	callback(results);
      }        
   }); //outer sql;
}



var getRemoteConnection = function(details){

  var connection = mysql.createConnection({
    host     : details.host,
    user     : details.uid,
      password : details.password,
      database : details.name,
      //uncomment socketPath in case of mac osx
      //socketPath: "/Applications/MAMP/tmp/mysql/mysql.sock"
    });   
    
    return connection;    

}


/**
*   DB: {host: "", name: "", uid: "", password: "", object:"" }
*   returns -1000 when error after tries times
*/
reconnect_All_RemoteDB = function(token, callback){ //
  
  var object = getObject(token);
  
  console.log('Trying to connect sql. in api.js');
  
 // var connection = getRemoteConnection(object.DB.old_db_events);  
  //object.DB.old_db_events.object = connection;

  var connection = getRemoteConnection(object.DB.db_events);  
  object.DB.db_events.object = connection;

  var connection = getRemoteConnection(object.DB.db_rooms);  
  object.DB.db_rooms.object = connection;    
  
  console.log("connected to all remote db and saved the db instance in global variable");      

  updateObject(object, token);       
  
}



/**
**  Main function for rout login
*/
exports.login = function (req, res) {  

	if(!connection){
		//connection = reconnect("dst1");
	}	
  if(req.body.password==="dst")
  {
    // from DB authenticated now, dummy --> if true
     var profile = {
       first_name: req.body.username,
       last_name: 'gupta',
       email: 'pulkit@pulkit.eu',
       id: 123,
       remoteDB_IP: "",
       fid: ""
     };  
     // We are sending the profile inside the token
    var token = jwt.sign(profile, jwt_secret, { expiresInMinutes: 60*5 });

    console.log("body is "+req.body.username);    
     
    var userData = profile;
    userData.token = token;
    userData.session = true;

    //take it from database of web app while sign in and making session, Currently its hard coded

    // and then we will initialze the sql object from the host, db name, uid, password....

    userData.DB = {old_db_events: {host: "localhost", name: "dst1", uid: "root", password: "pulkit.eu", object: null},
                   db_rooms: {host: "localhost", name: "db_rooms", uid: "root", password: "pulkit", object: null},
                   db_events:{host: "localhost", name: "db_events", uid: "root", password: "pulkit", object: null}  }

    online_user.push(userData); //push iin array 
    reconnect_All_RemoteDB(token);  

    res.json({token: token});
    console.log("json sent with token");

  }
  else
  {    // infuture will have the wrong passwod or username
    res.json({error: "wrong"});
    console.log("json not sent instead a warning is sent"); 
  }

};

exports.logout = function (req, res){
	if(getObject(req.body.token).session === true){		
		//delete the object containing this token from live user
	}
	else{
		console.log("No session! session was not created or already destroyed");
	}
};




