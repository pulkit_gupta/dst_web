// getDataG2


exports.getDataG2 = function (req, res) {
    //value from database will be fetched later on 
  var userData = getObject(req.body.token);
  if( userData.session === true){
      //get value stored earlier      
      sync_event_DB(userData.DB.db_events.object, function(dataObj){ 

        userData.remoteInfo = dataObj;
        //updte the database updated timestamp value to show to user here! 
        userData.series2 = g2_make_event_obj(dataObj);
        
        if(updateObject(userData, req.body.token)){
          console.log("updated the object with data of series 2");
        }       
        console.log("Data successfully sent and first value is "+userData.series2[0].name);
        res.json(userData.series2);  
     });        
                 
  }
  else
  {    // No session, only for Hackers
    res.json({data_error: "wrong"});
    console.log("Data json not sent instead a warning is sent"); 
  }

};


var g2_get_intv_array = function(userInfo, id){ 
  var i =0, intvArray = new Array();
  for(i=0;i<userInfo.event_data.length; i++)
  {
    if(userInfo.event_data[i].event_id == id)
    {
      intvArray.push({from: Math.round(userInfo.event_data[i].timestamp_start/1000), to: Math.round(userInfo.event_data[i].timestamp_end/1000)});                  
    }
  }
  //console.log("value returned is for event "+userInfo.event_name_array[id] +"is ->");
  //console.log(intvArray);
  return intvArray;
}

var g2_make_event_obj = function(userInfo, userID){ 
  var g2Obj=Array();
  
  //console.log("value of names are");
  //console.log(userInfo.event_name_array);
  var i =0, element=0;

  for(i=1;i<=userInfo.num_of_events; i++)   //giving 16 numbers
  {
    if(i==8|| i==12 || i==13)// || userInfo.event_data[i].event_id==12 || userInfo.event_data[i].event_id==13)
    {
        console.log("SKIPPING THE EVENT NAME"+ userInfo.event_name_array[i]);
    }
    else
    {
      g2Obj.push({id: userInfo.event_data[i].event_id, name: userInfo.event_name_array[i], intervals: g2_get_intv_array(userInfo, i)});
    }
    //console.log(g2Obj[i]);
    //console.log("\n");
  }
                  
  return g2Obj;
}
