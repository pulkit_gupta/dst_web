-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 15, 2014 at 07:01 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dst`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `event_id` int(11) NOT NULL,
  `event_name` text,
  `event_description` text,
  `type` text,
  `fid` int(11) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `events_data`
--

CREATE TABLE IF NOT EXISTS `events_data` (
  `fid` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `framework`
--

CREATE TABLE IF NOT EXISTS `framework` (
  `fid` int(11) NOT NULL,
  `name` text,
  `map_id` int(11) DEFAULT NULL,
  `db_ip_address` text,
  `db_name` text,
  `db_password` text,
  `db_last_sync` timestamp NULL DEFAULT NULL,
  `added_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `framework_datalog`
--

CREATE TABLE IF NOT EXISTS `framework_datalog` (
  `fid` int(11) NOT NULL,
  `images` text,
  `video` text,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `location_id` int(11) NOT NULL,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `z` int(11) DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fid` int(11) DEFAULT NULL,
  `firstname` text,
  `lastname` text,
  `street` text,
  `city` text,
  `state` text,
  `postal` text,
  `country` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `passwordreset` text NOT NULL,
  `password` text NOT NULL,
  `created_on` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fid`, `firstname`, `lastname`, `street`, `city`, `state`, `postal`, `country`, `email`, `mobile`, `active`, `passwordreset`, `password`, `created_on`) VALUES
(3, 0, 'Pulkit', 'Gupta', '', 'Delhi', 'Delhi', '110034', 'Canada', 'pulkit.itp@gmail.com', '919958140688', 1, '219c00481c2150a09a750eaecc15ac6c', '96ac59657aecb0e65afb4aaa0bdb7cfc', '1355002773002'),
(4, 0, 'Ankit', 'Gupta', 'Wz-624 Ivrd Floor', 'Delhi', 'Delhi', '110034', 'United Kingdom', 'pulkit.rnd@gmail.com', '9210061566', 1, '13fdf413915da55bf4009d60f87b87d2', '4873e54f1b9a84f05dc858de98fbb961', '1350686654163'),
(5, 0, 'ankur', 'sethi', 'wwsss', 'khjb', 'khb', 'kjb', 'United Kingdom', 'ankur@mailinator.com', '', 1, '9703c6e58b65106aa00e751584c8c4f3', 'b8bb32a953f0d051f8df58ecbce17caf', '1350847566125'),
(6, 0, 'Pulkit', 'g', 'gg', 'g', 'gg', 'g', '', 'pulki@malinator.com', '', 0, '119fe462722c8d05f9f8bf5d796f43a5', 'a33a7dca3415af291a108717727e11ce', '1350909260961'),
(7, 0, 'Pulkit', 'g', 'gg', 'g', 'gg', 'g', '', 'pulkit@malinator.com', '', 0, 'c0b6eb2f7fa48de3401a986a804890a4', '3f86805dbe055fc08893b60c9e78df65', '1350909291170'),
(8, 0, 'pp', 'pp', 'pp', 'pp', 'pp', 'pp', '', 'apulkit@malinator.com', '', 0, '45155aef085580505f31a4ff827d6d90', 'a42c7b51e4c0f3ea12cb97334a33818e', '1350909772737'),
(9, 0, 'pp', 'pp', 'pp', 'pp', 'pp', 'pp', '', 'awpulkit@malinator.com', '', 0, '778b3cf5b0b2a373ea4d1daa949bdb52', 'e1889562e2b0e5fc822433f21a2badfa', '1350910014525'),
(10, 0, 'Pulkit', 'p', 'p', 'p', 'p', 'p', '', 'pulkitg@malinator.com', 'p', 0, '4277e7dba4a61225f53500927088e3f1', '237a9a38d215702d044faa71f71f6b8b', '1351607673210'),
(11, 0, 'p', 'p', 'p', 'p', 'p', 'p', '', 'p@p.com', 'p', 0, '1b93a10a12e060b8146b820e4e279cdb', 'f56d6b77ce8016aa95645f6ba3ca97ba', '1351698398766'),
(12, 0, 'p', 'p', 'p', 'p', 'p', 'p', 'India', 'ppp@mailinator.com', '', 1, '8cc5df890ff0a440927f40378d43285c', '4f432c2092284bcc3ae4ef16dea2eb23', '1351947147177'),
(13, 0, 'p', 'p', 'p', 'p', 'p', 'p', 'India', 'jj@mailinator.com', '', 1, 'b06680968b4ac0ed1a9fde4a97140cf8', 'ea01bf24118cee6287140c183423843c', '1351947241434'),
(14, 0, 'p', 'p', 'p', 'p', 'p', 'p', '', 'p@mailinator.com', '', 0, 'a4125680746924adf23bc65b5b870f64', 'c71219b5b86359d7b04cc7f1a0000d9c', '1351947605699'),
(15, 0, 'p', 'p', 'p', 'p', 'p', 'p', '', 'abc@mailinator.com', '', 1, '550256aaed8726e7c912cded7e7a96b8', '2ab35afc2c8b9fef715dd011bcb01310', '1351947822940'),
(16, 0, 'P', 'p', '', '', '', '', '', 'kk@mailinator.com', '', 0, '7532a19f20e249c07db77d5959c137b6', '8947cdd692d3c1bfe4088fd8bbc8adc7', '1352132100772'),
(17, 0, 'p', 'pp', '', '', '', '', '', 'p1@mailinator.com', '44', 1, '6230bb38f6e0eb41327a2f34033cd089', '88736333412be90f801448a9f8ab6607', '1352132213196'),
(18, 0, 'pp', 'pp', '', '', '', '', '', 'aap@mailinator.com', '', 0, '7d9914bfb9b6a22eabc4f9c8ecb759aa', '2f2217e7ace273e6ba83cf14740bc789', '1352314735430'),
(19, 0, 'p', 'p', '', '', '', '', '', 'pulkit@mailinator.com', '', 0, 'a0b373bb53fe58366dde6787c25a64f4', '45e84e7730e16baa619c8eccb1829c37', '1352410595628'),
(20, 0, 'pl', 'pl', '', '', '', '', '', 'pk@mailinator.com', '9958140688', 1, '975a561a118782009512fd1fcd3c95a5', 'faac5ee5350ad4762916d75afda07c07', '1352563865649');

-- --------------------------------------------------------

--
-- Table structure for table `user_frameworks`
--

CREATE TABLE IF NOT EXISTS `user_frameworks` (
  `uid` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
